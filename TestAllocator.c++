// -----------------------------------------
// projects/c++/allocator/TestAllocator1.c++
// Copyright (C) 2017
// Glenn P. Downing
// -----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/Primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type  x;
    const size_type       s = 10;
    const value_type      v = 2;
    const pointer         b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

TEST(TestAllocator2, const_index) {
    const my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}

TEST(TestAllocator2, index) {
    my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 92);
}

// --------------
// TestAllocator3
// --------------

template <typename A>
struct TestAllocator3 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    100>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator3, my_types_2);

TYPED_TEST(TestAllocator3, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator3, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 10;
    const value_type     v = 2;
    const pointer        b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(Test_My_Allocate1, test_1) {
    my_allocator<double, 10000> allocator;
    allocator.allocate(8);
    ASSERT_EQ(allocator[0], -64);
}
TEST(Test_My_Allocate1, test2) {
    my_allocator<double, 10000> allocator;
    allocator.allocate(5);
    allocator.allocate(3);
    ASSERT_EQ(allocator[48], -24);

}
TEST(Test_My_Allocate1, test5) {
    my_allocator<double, 10000> allocator;
    allocator.allocate(5);
    int * p = (int *)allocator.find_busy_block(1);
    ASSERT_EQ(*p, -40);
}
TEST(Test_My_Allocate1, test3) {
    my_allocator<double, 10000> allocator;
    allocator.allocate(5);
    double * p = allocator.find_busy_block(1);
    allocator.deallocate(p,-1);
    ASSERT_EQ(allocator[0], 9992);
}

TEST(Test_My_Allocate1,test4) {
    my_allocator<double, 10000> allocator;
    allocator.allocate(5);
    allocator.allocate(3);
    double * p = allocator.find_busy_block(2);
    allocator.deallocate(p,-1);
    ASSERT_EQ(allocator[48], 9944);
}

TEST(Test_My_Allocate1,test7) {
    my_allocator<double, 10000> allocator;
    allocator.allocate(5);
    allocator.allocate(3);
    double * p = allocator.find_busy_block(1);
    allocator.deallocate(p,-1);
    ASSERT_EQ(allocator[0], 40);
}
TEST(Test_My_Allocate1,test6) {
    my_allocator<double, 10000> allocator;
    allocator.allocate(5);
    allocator.allocate(3);
    double * p = allocator.find_busy_block(1);
    allocator.deallocate(p,-1);
    p = allocator.find_busy_block(1);
    allocator.deallocate(p,-1);
    ASSERT_EQ(allocator[0], 9992);
}
TEST(TestAllocator3, print_allocate_only) {
    my_allocator<double, 1000> my_allocator;
    my_allocator.allocate(5);
    my_allocator.allocate(3);
    string s = "-40 -24 912 \n";
    ostringstream w;
    my_allocator.print(w);
    ASSERT_EQ(s, w.str());
}
TEST(TestAllocator3, print_allocate_deallocate) {
    my_allocator<double, 1000> my_allocator;
    my_allocator.allocate(5);
    my_allocator.allocate(3);
    my_allocator.deallocate(my_allocator.find_busy_block(1), -1);
    string s = "40 -24 912 \n";
    ostringstream w;
    my_allocator.print(w);
    ASSERT_EQ(s, w.str());
}
TEST(TestAllocator3, valid) {
    my_allocator<double, 1000> my_allocator;
    my_allocator.allocate(5);
    my_allocator.allocate(3);
    ASSERT_EQ(my_allocator.valid(), true);
}
