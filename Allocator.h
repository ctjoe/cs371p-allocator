// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream> // cout, endl

#include <gtest/gtest.h> //friend_test

#define SENTINEL_SIZE sizeof(int)

using namespace std; // delete


// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }


private:

    FRIEND_TEST(TestAllocator3, valid);
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * validates the memory blocks. Checks if neighboring free blocks are
     * coalesced and if left_sentinel values match right_sentinel values.
     *
     * @return bool - true if it is valid and false if it is not valid.
     */
    bool valid () const {
        int left_sentinel = (*this)[0];
        // index into the memory, one byte at a time.
        std::size_t index = 0;

        while (index < N) {
            // check if neighboring free blocks are coalesced
            if (left_sentinel > 0
                    && index > 0
                    && (*this)[index - SENTINEL_SIZE] > 0 ) {
                return false;
            }

            // index of right sentinel
            index += abs(left_sentinel) + SENTINEL_SIZE;
            int right_sentinel = (*this)[index];

            if (right_sentinel != left_sentinel) {
                return false;
            }

            // update index and left_sentinel
            index += SENTINEL_SIZE;
            left_sentinel = (*this)[index];
        }

        assert (index == N);
        return true;
    }




public:
    // ------------
    // constructors
    // ------------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * SENTINEL_SIZE)
     */
    my_allocator () {
        // Check if N is greater than the minimum space that can be allocated.
        if (N < (sizeof(T) + (2 * SENTINEL_SIZE))) {
            throw bad_alloc();
        }

        // Initializes the memory with the initial free size.
        int free_size = N - 2 * SENTINEL_SIZE;
        (*this)[0] = free_size;
        (*this)[N-SENTINEL_SIZE] = free_size;

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // read_request
    // --------

    /**
     * reads memory allocation and deallocation requests.
     * @param r - istream object for reading input.
     * @return a vector of allocate and deallocate requests for a test.
     */

    vector<int> read_request (istream& r) {
        vector<int> values;

        string s;
        while (getline(r, s)) {
            // if test case input has ended
            if (s.empty()) {
                break;
            }

            istringstream sin(s);
            int val;
            sin >> val;
            values.push_back(val);
        }

        assert(values.size() > 0);
        return values;
    }

    // --------
    // print
    // --------

    /**
     * prints the left sentinel values for each block
     * @param w - ostream object for writing output.
     * @return void
     */
    void print(ostream& w) {
        int left_sentinel = (*this)[0];
        std::size_t index = 0;

        while (index < N) {
            // prints the left_sentinel values for each block.
            w << left_sentinel << "\n "[index + SENTINEL_SIZE < N];
            index += abs(left_sentinel) + SENTINEL_SIZE;
            index += SENTINEL_SIZE;
            left_sentinel = (*this)[index];

        }

        assert (index == N);
        w << endl;
    }

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * SENTINEL_SIZE)
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     * @param val - number of blocks to be allocated.
     * @return a pointer to the start of the bytes allocated.
     */
    pointer allocate (size_type val) {

        assert (val > 0);

        int index = 0;
        int allocated_memory_index = 0;
        int min_allocation_space = 2 * SENTINEL_SIZE + sizeof(T);
        size_type num_bytes = val * sizeof(T);

        if (num_bytes > N || num_bytes < 0 || (int) N < min_allocation_space) {
            throw bad_alloc();
        }

        while ((size_t) index < N) {

            int sentinel = (*this)[index];

            // If there is enough space in the current block.
            if (sentinel >= (int)num_bytes) {

                if (sentinel - (int)num_bytes >= min_allocation_space) {
                    // Create a busy block sentinel.
                    int new_sent = (int) num_bytes * -1;
                    (*this)[index] = new_sent;

                    allocated_memory_index = index + SENTINEL_SIZE;

                    // Update the right sentinel for the block.
                    index += num_bytes + SENTINEL_SIZE;
                    (*this)[index] = new_sent;

                    // If there is space left after allocation, update sentinels.
                    index += SENTINEL_SIZE;
                    (*this)[index] = sentinel - num_bytes - 2 * SENTINEL_SIZE;

                    index += SENTINEL_SIZE;
                    index += sentinel - num_bytes - 2 * SENTINEL_SIZE;

                    (*this)[index] = sentinel - num_bytes - 2 * SENTINEL_SIZE;
                    index += SENTINEL_SIZE;

                } else {
                    (*this)[index] = sentinel * - 1;
                    allocated_memory_index = index + SENTINEL_SIZE;
                    index += sentinel + SENTINEL_SIZE;
                    (*this)[index] = sentinel * - 1;
                    index += SENTINEL_SIZE;
                }

                // if required memory has been allocated, break.
                break;
            }
            else {
                index += abs((int)sentinel) + 2 * SENTINEL_SIZE;
            }
        }

        assert(valid());

        // If memory was allocated.
        if (allocated_memory_index != 0) {
            return (pointer)(&(*this)[allocated_memory_index]) ;
        }
        else {
            return nullptr;
        }
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v); // this is correct and exempt
        assert(valid());
    } // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * @param p - pointer to the sentinel of the memory to be freed.
     * @param t - if 0, free the whole block starting at the sentinel
     *            if > 0, free the memory at the pointer.
     * @return void
     */
    void deallocate (pointer p, size_type t) {

        char * byte_pointer = reinterpret_cast<char*> (p);

        // validate the pointer argument passed to the function.
        if (byte_pointer == nullptr
                || byte_pointer < a
                || byte_pointer > a + N) {
            throw invalid_argument("Invalid pointer");
        }

        int size = t * sizeof(T);

        if (size > 0) {
            int free_size = N - 2 * SENTINEL_SIZE;
            (*this)[0] = free_size;
            (*this)[N-SENTINEL_SIZE] = free_size;
        }
        else {
            int * sentinel_pointer = reinterpret_cast<int*> (byte_pointer);
            // make the sentinel value positive
            *sentinel_pointer = *sentinel_pointer * -1;

            /* pointer to the right sentinel value of the block that is found
             * to the left of the block to be deallocated.
             * This is for checking possible coalesce in deallocation
             */
            int * left_sentinel_pointer =
                reinterpret_cast<int*> (byte_pointer - SENTINEL_SIZE);

            // check if the left block can be coalesced.
            if (reinterpret_cast<char*> (left_sentinel_pointer) >= a
                    && *left_sentinel_pointer > 0) {

                // New sentinel value after coalescing
                int new_sentinel = *left_sentinel_pointer
                                   + *sentinel_pointer
                                   + 2 * SENTINEL_SIZE;

                /* pointer to the left sentinel value of the block that is found
                 * to the left of the block to be deallocated. */
                int * extreme_left_sentinel_pointer =
                    reinterpret_cast<int*> ((char*)left_sentinel_pointer
                                            - *left_sentinel_pointer
                                            - SENTINEL_SIZE);

                // update the sentinel value to the new sentinel after coalescing
                *(extreme_left_sentinel_pointer) = new_sentinel;

                // find the position of the right sentinel value after coalescing
                extreme_left_sentinel_pointer =
                    reinterpret_cast<int*> ((char *) extreme_left_sentinel_pointer
                                            + SENTINEL_SIZE
                                            + *extreme_left_sentinel_pointer);
                *extreme_left_sentinel_pointer = new_sentinel;
            }
            // else if left side block does not need coalescing.
            else {
                // deallocate the requested block
                sentinel_pointer =
                    reinterpret_cast<int*> (byte_pointer
                                            + (*sentinel_pointer)
                                            + SENTINEL_SIZE);
                *sentinel_pointer = *sentinel_pointer * -1;
            }

            /* pointer to the left sentinel value of the block that is found
             * to the right of the block to be deallocated.
             * This is for checking possible coalesce in deallocation
             */
            int * right_sentinel_pointer =
                reinterpret_cast<int*> (byte_pointer
                                        + *((int *)byte_pointer)
                                        + 2 * SENTINEL_SIZE);

            // check if we can coalesce the block to the right of the block to
            // be deallocated
            if (reinterpret_cast<char*> (right_sentinel_pointer) < a + N
                    && *right_sentinel_pointer > 0) {

                // new sentinel after coalescing
                int new_sentinel = *right_sentinel_pointer
                                   + *(right_sentinel_pointer - 1)
                                   + 2 * SENTINEL_SIZE;

                /* pointer to the left sentinel value of the block that is found
                 * to the right of the block to be deallocated. */
                int * extreme_right_sentinel_pointer =
                    reinterpret_cast<int*> ((char *)right_sentinel_pointer
                                            + SENTINEL_SIZE
                                            + *right_sentinel_pointer);

                *extreme_right_sentinel_pointer = new_sentinel;

                // left matching sentinel pointer to extreme_right_sentinel_pointer
                int * extreme_left_sentinel_pointer =
                    reinterpret_cast<int*> ((char *)extreme_right_sentinel_pointer
                                            - *extreme_right_sentinel_pointer
                                            - SENTINEL_SIZE);

                *extreme_left_sentinel_pointer = new_sentinel;
            }
        }

        assert(valid());
    }

    // ----------
    // find_busy_block
    // ----------

    /**
     * O(1) in space
     * O(n) in time
     * goes through memory blocks to find the nth busy block.
     * @param n - the nth busy block to be freed.
     * @return the pointer to the nth busy block or null if no busy block.
     */

    pointer find_busy_block(int n) {
        // n needs to be atleast 1
        assert(n > 0);

        // index into the memory one byte at a time
        int index = 0;
        pointer busy_block_pointer = nullptr;
        while(n != 0 ) {
            if((*this)[index] < 0 ) {
                --n;
                busy_block_pointer = (pointer)&((*this)[index]);
            }
            int to_increment = abs((*this)[index]) + 2 * SENTINEL_SIZE;
            index += to_increment;
        }

        return busy_block_pointer;
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     * @param p - pointer to be destroyed.
     * @return void
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    /**
     * O(1) in space
     * O(1) in time
     * @param i - index into the memory to be read as sentinel.
     * @return int& of sentinel at index i
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     * @param i - index into the memory to be read as sentinel.
     * @return int& of sentinel at index i
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }
};

#endif // Allocator_h
