// ---------------------------------------
// projects/c++/allocator/RunAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ---------------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <fstream>  // ifstream
#include <iostream> // cout, endl, getline
#include <string>   // s
#include <vector>   // vector
#include <sstream>  // istringstream

#include "Allocator.h"

using namespace std;

#define HEAP_SIZE 1000
#define DATA_SIZE sizeof(double)

// ----
// main
// ----

int main () {

    int number_of_tests = 0;
    string s;
    getline(cin, s);
    istringstream sin(s);
    // read number of test cases
    sin >> number_of_tests;

    getline(cin, s); // empty line

    while (number_of_tests > 0) {
        my_allocator<double, HEAP_SIZE> allocator;
        vector<int> result = allocator.read_request(cin);

        for (int val : result) {
            // deallocate
            if (val < 0) {
                allocator.deallocate(allocator.find_busy_block(val * -1),-1);
            }
            // allocate
            else {
                allocator.allocate(val);
            }
        }
        allocator.print(cout);
        --number_of_tests;
    }

    return 0;
}
